export prefix     = ~/.config/bspwm
export configdir = $(prefix)/share/bspwm-custom

#configs: _bspwm _polybar _sxhkd
_bspwm:

	$(MAKE) -C bspwm
_polybar:

	$(MAKE) -C polybar
_sxhkd:

	$(MAKE) -C sxhkd

_scripts:

	$(MAKE) -C scripts

make_configs: _bspwm _sxhkd _scripts

make_xsession_entry:

	$(MAKE) -C xsession_entry

all: make_configs make_xsession_entry

install_configs:

	mkdir -p $(configdir)

	cp -r bspwm $(configdir)
	cp -r polybar $(configdir)
	cp -r sxhkd $(configdir)
	cp -r scripts $(configdir)

	rm $(configdir)/*/Makefile
	chmod +x $(configdir)/bspwm/bspwmrc
	chmod +x $(configdir)/scripts/*

install_xsession_entry:

	mkdir -p $(prefix)/share/xsessions
	cp xsession_entry/* $(prefix)/share/xsessions
	rm $(prefix)/share/xsessions/Makefile

install: install_configs install_xsession_entry
